﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HCM_dotnet
{
    class Program
    {
        static void Main(string[] args)
        {
            HCM hcm = new HCM();
            Console.Write(hcm.run());
        }
    }

    class HCM
    {
        const int CLUSTER = 2;
        const int DATA = 4;
        const int COORD = 4; /*No of input output */
        const double e = 1.0e-6;

        public static int[,] U = new int[CLUSTER, DATA];
        public static int[,] U_OLD = new int[CLUSTER, DATA];
        public static double[,] v = new double[CLUSTER, COORD];
        public static double[,] d = new double[CLUSTER, DATA];
        public static double[,] x = new double[DATA, COORD];
        public static double[] array = new double[80];

        StringBuilder sb = new StringBuilder();

        public String run()
        {
            int i, j, k, b, iter = 0;
            double num, den, t_num, t_den, min;
            double t_dist, t2_dist;
            double Jm, Old_Jm, Jm_error;
            int count = 0;
            FileStream fp11 = File.Open("text.txt",FileMode.OpenOrCreate); //파일 데이터 경로
            StreamReader fp1 = new StreamReader(fp11); //파일을 읽을 수 있게 가져오기
            string line;
            //파일에서 데이터 값을 스트링으로 받아오기 때문에 string 변수를 잡아 줌
            //파일을 불러와서 null 값을 만날떄 까지 읽어오는 소스
            //파일의 내용을 차원 배열에 넣어준다.

            while ((line = fp1.ReadLine()) != null)
            {
                array[count] = Convert.ToDouble(line);
                //string으로 받아온 데이터를 double 형으로 바꿔준다.
                count++;
            }

            //count 값을 다시 초기화
            count = 0;

            // 일차원 배열에 있는 데이터를 이차원 배열에 다시 넣어준다.
            // 이차원 배열에 있는 내용 출력
            for (i = 0; i < DATA; i++)
                for(j = 0; j < COORD; j++){
                    x[i,j] = array[count];
                    sb.AppendLine("x : [" + i + ","  + j + "] = " + x[i,j]); //임의 변형
                    count++;
                }

            //Make U - matrix

            for(i = 0; i < CLUSTER; i++)
                for(j = 0; j < DATA; j++){ //initialize U
                    U[i,j] = 0;
                }
            
            for (i = 0; i < DATA; i++){
                U[0,i] = 1;
            }

            for (i = 1; i < CLUSTER; i++){
                U[i, DATA - i] = 1; //임의로 데이터를 준다.
                U[0, DATA - i] = 0;
            }

            do
            {
                iter++;

                //calculate center of cluster

                for (i = 0; i < CLUSTER; i++)
                    for (j = 0; j < COORD; j++){
                        num = 0;
                        den = 0.1;

                        for (k = 0; k < DATA; k++)
                        {
                            t_num = 0;
                            t_den = 0;
                            t_num = U[i,k] * x[k,j];
                            t_den = U[i, k];
                            num = num + t_num;
                            den = den + t_den;
                        }

                        v[i,j] = num / den;
                    }
                
                //Calculate distance detween data and cluster center

                for(i = 0; i < CLUSTER; i++)
                    for(j =0; j < DATA; j++){
                        t2_dist = 0;

                        for(k = 0; k < COORD; k++){
                            t_dist = (x[j,k] - v[i,k]) * (x[j,k] - v[i,k]);
                            t2_dist = t2_dist + t_dist;
                        }

                        d[i, j] = Math.Sqrt(t2_dist);
                    }
                
                Jm = 0; //calculation of new Jm

                for(k = 0; k < DATA; k++)
                    for(i = 0; i < CLUSTER; i++){
                        Jm = Jm + U[i,k] * Math.Pow(d[i,k],2);
                    }
                
                for(i = 0; i < CLUSTER; i++) 
                    for(j = 0; j < DATA; j++){ //change of new U
                        U_OLD[i,j] = U[i,j];
                        U[i,j] = 0;
                    }
                
                //upgrade u matrix

                for(i = 0; i < DATA; i++){
                    //calculater of new U
                    min = 0;
                    b = 0;
                    min = d[0, i];

                    for(j = 1; j < CLUSTER; j++){
                        if(min > d[j,i]){
                            min = d[j,i];
                            b = j;
                        }
                    }
                    U[b, i] = 1;
                }

                for(i = 0; i < CLUSTER; i++)
                    for(j = 0; j < COORD; j++){ //calculation of vector center(v)
                        num = 0;
                        den = 0;
                        
                        for(k = 0; k < DATA; k++){
                            t_num = 0;
                            t_den = 0;
                            t_num = U[i, k] * x[k,j];
                            t_den = U[i, k];
                            num = num + t_num;
                            den = den + t_den;
                        }
                        v[i,j] = num / den;
                    }
                
                Old_Jm = Jm; //difference of Jm
                Jm = 0.0;

                for(k = 0; k < DATA; k++)
                    for(i = 0; i < CLUSTER; i++){
                        Jm = Jm + U[i,k] * Math.Pow(d[i,k], 2);
                    }
                
                Jm_error = Math.Abs(Old_Jm - Jm);
            } while (e < Jm_error);

            for(i = 0; i < CLUSTER - 1; i++){
                sb.Append("{");
                for(j = 0; j < COORD; j++){
                    sb.Append(v[i,j] + " ");
                }
                sb.AppendLine("}\n");
            }

            for(i = 0; i < CLUSTER; i++){
                for(j = 0; j < DATA; j++){
                    sb.Append(U[i,j]);
                }
                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
